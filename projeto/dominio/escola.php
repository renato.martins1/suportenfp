<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of escola
 *
 * @author Renat
 */
class escola {
    //put your code here
    
    private $nome;
    private $id;
    private $ua;
    private $email;
    private $telefone;
    private $caixa;
    private $endereco;
    private $bairro;
    private $cidade;
    
    function __construct($nome, $id, $ua, $email, $telefone, $caixa, $endereco, $bairro, $cidade) {
        $this->nome = $nome;
        $this->id = $id;
        $this->ua = $ua;
        $this->email = $email;
        $this->telefone = $telefone;
        $this->caixa = $caixa;
        $this->endereco = $endereco;
        $this->bairro = $bairro;
        $this->cidade = $cidade;
    }

        
    function getNome() {
        return $this->nome;
    }

    function getId() {
        return $this->id;
    }

    function getUa() {
        return $this->ua;
    }

    function getEmail() {
        return $this->email;
    }

    function getTelefone() {
        return $this->telefone;
    }
    
    function getCaixa() {
        return $this->caixa;
    }
    
    function getEndereco() {
        return $this->endereco;
    }

    function getBairro() {
        return $this->bairro;
    }

    function getCidade() {
        return $this->cidade;
    }

    
    
    function setNome($nome) {
        $this->nome = $nome;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUa($ua) {
        $this->ua = $ua;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    function setCaixa($caixa) {
        $this->caixa = $caixa;
    }

    function setEndereco($endereco) {
        $this->endereco = $endereco;
    }

    function setBairro($bairro) {
        $this->bairro = $bairro;
    }

    function setCidade($cidade) {
        $this->cidade = $cidade;
    }



}
