<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of telefone
 *
 * @author Renat
 */
class telefone {
    //put your code here
    
    private $telefone1;
    private $telefone2;
    
    function __construct($telefone1, $telefone2) {
        $this->telefone1 = $telefone1;
        $this->telefone2 = $telefone2;
    }
    
    function getTelefone1() {
        return $this->telefone1;
    }

    function getTelefone2() {
        return $this->telefone2;
    }

    function setTelefone1($telefone1) {
        $this->telefone1 = $telefone1;
    }

    function setTelefone2($telefone2) {
        $this->telefone2 = $telefone2;
    }



}
