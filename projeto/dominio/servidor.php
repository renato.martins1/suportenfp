<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of servidor
 *
 * @author Renat
 */
class servidor {
    //put your code here
    
    function __construct($nome, $escola) {
        $this->nome = $nome;
        $this->escola = $escola;
    }

    private $id;
    private $nome;
    private $escola;
    
    function getNome() {
        return $this->nome;
    }

    function getEscola() {
        return $this->escola;
    }
    
    function getId() {
        return $this->id;
    }

    
    function setNome($nome) {
        $this->nome = $nome;
    }

    function setEscola($escola) {
        $this->escola = $escola;
    }

    function setId($id) {
        $this->id = $id;
    }



}
