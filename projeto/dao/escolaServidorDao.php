<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of escolaServidorDao
 *
 * @author Renat
 */
class escolaServidorDao {

    //put your code here
    private $con;

    function __construct() {
        $this->con = new Conexao();
    }

    public function consultar() {
        $con = $this->con->getConexao();


        $sql = "SELECT a.nome,b.nome escola,b.email,b.caixa,b.id idescola,b.ua,a.id idservidorcaixa  FROM servidorcaixa a join escola b on a.escolaid = b.id order by a.id desc";

        $stmp = $con->prepare($sql);


        $stmp->execute();

        $resultado = $stmp->fetchAll();



        foreach ($resultado as $value) {

            $escola = new escola($value['escola'], $value['idescola'], $value['ua'], $value['email'], "", $value['caixa'],"","","");

            $servidor = new servidor($value['nome'], $escola);
            
            $servidor->setId($value['idservidorcaixa']);

            $entidades[] = $servidor;
        }

        if (isset($entidades)) {
            return $entidades;
        } else {
            return "";
        }
    }

    public function salvar($servidor) {

        $con = $this->con->getConexao();



        $stmt = $con->prepare(
                "INSERT INTO SERVIDORCAIXA("
                . "NOME,"
                . "ESCOLAID)"
                . " VALUES ("
                . "?,"
                . "?)"
        );



        $nome = $servidor->getNome();
        $escolaid = $servidor->getEscola()->getId();

        $stmt->bindParam(1, $nome);
        $stmt->bindParam(2, $escolaid);


        $stmt->execute();

        return $res['status'] = 'OK';
    }

    public function limpar($servidor) {

        $con = $this->con->getConexao();


        if (is_object($servidor)) {
            $complemento = " WHERE ID = '" . $servidor->getId() . "'";
        }else{
            $complemento = "";
        }

        $stmt = $con->prepare("DELETE FROM SERVIDORCAIXA $complemento");

        $stmt->execute();
        return $res['status'] = 'OK';
    }

}
