<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of usuarioDao
 *
 * @author renato.martins
 */

include_once 'Conexao.php';


class usuarioDao {
    
    private $con;

    function __construct() {

        $this->con = new Conexao();
    }
    
    public function consultar($id,$nome){
        $con = $this->con->getConexao();

        $entidades = null;
        
        $sql = "SELECT * FROM USUARIO WHERE NOME = ? ";
        
        $dadosArray[] = $nome;
        
        $stmt = $con->prepare($sql);
        
        $stmt->execute($dadosArray);
        
        $resultado = $stmt->fetchAll();
        
        foreach ($resultado as $value) {
            $usuario = new usuario($value['nome'], $value['grupo'], $value['senha']);
            $entidades[] = $usuario;
        }
        
        return $entidades;
    }
}
