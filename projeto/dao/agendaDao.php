<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of agendaDao
 *
 * @author renato.martins
 */
include 'Conexao.php';

class agendaDao {

    //put your code here
    private $con;

    function __construct() {

        $this->con = new Conexao();
    }

    public function salvar($entidade) {
        $con = $this->con->getConexao();

        $stmt = $con->prepare("INSERT INTO AGENDA("
                . "nome," //1
                . "centro," //2
                . "projetor,"//3
                . "qtdenotebooks,"//4
                . "qtdemicrofones,"//5
                . "p2,"//6
                . "evento,"//7
                . "datainicio,"//8
                . "datafim,"//9
                . "qtdecomputadores,"//10
                . "internet,"//11
                . "word," //12
                . "status,"//13
                . "datarequisicao," //14
                . "outro_sala_5," //15
                . "ambiente_recurso)"//16
                . " VALUES("
                . "?," //1
                . "?," //2
                . "?," //3
                . "?," //4
                . "?," //5
                . "?," //6
                . "?," //7
                . "?," //8
                . "?," //9
                . "?," //10
                . "?," //11
                . "?," //12
                . "?," //13
                . "?," //14
                . "?," //15
                . "?)"); //16

        $nome = $entidade->getPessoa()->getNome();
        $centro = $entidade->getNucleo();
        $anfiteatro = $entidade->getAmbienteCarroOficial()->getAnfiteatro();
        $sala4 = $entidade->getAmbienteCarroOficial()->getSala4();
        $carro = $entidade->getAmbienteCarroOficial()->getCarroOficial();

        $projetor = $entidade->getProjetor();
        $qtdeNotebooks = $entidade->getQtdeNotebooks();
        $qtdeMicrofones = $entidade->getQtdeMicrofones();
        $p2 = $entidade->getP2();

        $qtdeComputadores = $entidade->getAmbienteCarroOficial()->getSala5()->getQtdeComputadores();
        $internet = $entidade->getAmbienteCarroOficial()->getSala5()->getInternet();
        $word = $entidade->getAmbienteCarroOficial()->getSala5()->getWord();
        $sala5 = $entidade->getAmbienteCarroOficial()->getSala5()->getEscolha();
        $outroSala5 = $entidade->getAmbienteCarroOficial()->getSala5()->getOutro();
        $outro = $entidade->getAmbienteCarroOficial()->getOutro();

        /**
         * Verifica qual ambiente será gravado no banco de dados
         */
        if ($carro) {
            $ambiente_recurso = "carro";
        } elseif ($anfiteatro) {
            $ambiente_recurso = "anfiteatro";
        } elseif ($sala4) {
            $ambiente_recurso = "sala4";
        } elseif ($sala5) {
            $ambiente_recurso = "sala5";
        } elseif ($outro) {
            $ambiente_recurso = "outro";
        }

        $evento = $entidade->getEvento();

        $dataInicio = $entidade->getDataInicio();
        $dataFim = $entidade->getDataFim();
        $horaInicio = $entidade->getHoraInicio();
        $horaFim = $entidade->getHoraFim();

        $dataInicio = "$dataInicio $horaInicio:00";
        $dataFim = "$dataFim $horaFim:00";

        date_default_timezone_set('America/Sao_Paulo');
        $dataRequisicao = date("Y-m-d h:i:s");
        $status = "";


        $stmt->bindParam(1, $nome);
        $stmt->bindParam(2, $centro);
        $stmt->bindParam(3, $projetor);
        $stmt->bindParam(4, $qtdeNotebooks);
        $stmt->bindParam(5, $qtdeMicrofones);
        $stmt->bindParam(6, $p2);
        $stmt->bindParam(7, $evento);
        $stmt->bindParam(8, $dataInicio);
        $stmt->bindParam(9, $dataFim);
        $stmt->bindParam(10, $qtdeComputadores);
        $stmt->bindParam(11, $internet);
        $stmt->bindParam(12, $word);
        $stmt->bindParam(13, $status);
        $stmt->bindParam(14, $dataRequisicao);
        $stmt->bindParam(15, $outroSala5);
        $stmt->bindParam(16, $ambiente_recurso);

        $stmt->execute();

        return $res['status'] = 'OK';
    }

    public function consultar($id, $status,$ordenacao,$periodo) {
        $con = $this->con->getConexao();
        $comple = "";
        $dadosArray = "";
        
        /**
         * Preencha a variável da ordenação da consulta
         */
        
        switch ($ordenacao){
            case "dataHoraInicioAsc":
                $orderBy = "datainicio ASC";
                break;
            case "dataHoraInicioDesc":
                $orderBy = "datainicio DESC";
                break;
            case "dataHoraReqAsc":
                $orderBy = "datarequisicao ASC";
                break;
            case "dataHoraReqDesc":
                $orderBy = "datarequisicao DESC";
                break;
            
            default :
                $orderBy = "datainicio ASC";
                break;
        }
        
        
        
        switch ($periodo){
            case "dataInicioHoje":
                $periodoSql = "datainicio = (SELECT CURDATE())";
                break;
            case "dataInicioFuturo":
                $periodoSql = "datainicio > (SELECT CURDATE())";
                break;
            case "dataInicioPassado":
                $periodoSql = "datainicio < (SELECT CURDATE())";
                break;
            
            case "dataReqHoje":
                $periodoSql = "date_format(datarequisicao,'%Y-%m-%d') = (SELECT CURDATE())";
                break;
            
            case "dataReqPassado":
                $periodoSql = "date_format(datarequisicao,'%Y-%m-%d') < (SELECT CURDATE()) ";
                break;
            //date_format(datarequisicao,'%d/%m/%Y')
            default :
                $periodoSql  = "";
        }
        
        if(strlen($periodoSql)){
            $comple = "$periodoSql AND";
        }

        $sql = "SELECT * "
                . " FROM "
                . " AGENDA ";

        if (strlen($id) > 0) {
            $comple .= " IDAGENDA = ? AND";
            $dadosArray[] = $id;
        }

        if (strlen($status) > 0) {
            $comple .= " STATUS LIKE '$status' AND";
        }

        if (strlen($comple) > 0) {

            $comple = substr($comple, 0, -3);

            $sql = "$sql WHERE $comple";
        }

        $sql .= " ORDER BY $orderBy ";
        
        return $sql;

        $stmt = $con->prepare($sql);
        $stmt->execute();

        $resultado = $stmt->fetchAll();

        $entidades = null;

        $carroOficial = FALSE;
        $anfiteatro = FALSE;
        $sala4 = FALSE;
        $sala5 = FALSE;
        $outro = FALSE;

        foreach ($resultado as $value) {

            //$pessoa = new pessoa($value['pessoa']);
            $pessoa = new pessoa($value['nome']);

            $ambienteRecurso = $value['ambiente_recurso'];

            switch ($ambienteRecurso) {
                case "carrooficial":
                    $carroOficial = TRUE;
                    break;
                case "anfiteatro":
                    $anfiteatro = TRUE;
                    break;
                case "sala4":
                    $sala4 = TRUE;
                    break;
                case "sala5":
                    $sala5 = TRUE;
                    break;
                default :
                    $outro = $value['outro'];
                    break;
            }

            $sala5 = new sala5($value['qtdecomputadores'], $value['internet'], $value['word'], "", $value['outro_sala_5'], $value['sala5']);
            $ambienteCarroOficial = new ambienteCarroOficial($carroOficial, $anfiteatro, $sala4, $sala5, "", $outro);

            $agenda = new agenda($pessoa, $value['centro'], $value['projetor'], $value['qtdenotebooks'], $value['qtdemicrofones'], $value['p2'], $value['evento'], date("d/m/Y  h:i", strtotime($value['datainicio'])), date("d/m/Y  h:i", strtotime($value['datafim'])), date("h:i", strtotime($value['datainicio'])), date("h:i", strtotime($value['datafim'])), $ambienteCarroOficial, $value['status'], $value['datarequisicao'], $value['idagenda']);

            $entidades[] = $agenda;
        }

        return $entidades;
    }

    public function atualizarStatus($id, $status) {
        $con = $this->con->getConexao();

        $stmt = $con->prepare("UPDATE AGENDA"
                . " SET "
                . " STATUS = :status "
                . " WHERE "
                . " IDAGENDA = :idagenda");

        try {
            $res = $stmt->execute(array(':status' => $status, ':idagenda' => $id));
        } catch (PDOException $ex) {
            var_dump($e->getMessage());
        }
    }

    public function consultarAgendaEntreDatas($dataInicio, $horaInicio, $dataFim, $horaFim,$ambienteRecurso) {

        $con = $this->con->getConexao();
        $comple = "";

        $sql = "SELECT COUNT(*) TOTAL "
                . " FROM "
                . " AGENDA "
                . " WHERE "
                . " DATAINICIO >= ? AND"
                . " DATAFIM <= ? AND"
                . " STATUS = 'Deferido' AND"
                . " AMBIENTE_RECURSO = '$ambienteRecurso' ";



        $stmt = $con->prepare($sql);

        $dadosArray = array("$dataInicio $horaInicio", "$dataFim $horaFim");
        try {
            $stmt->execute($dadosArray);
        } catch (Exception $ex) {
            return $ex->getMessage();
        }

        $resultado = $stmt->fetchAll();

        $entidades = null;

        foreach ($resultado as $value) {

            $total = $value['TOTAL'];
        }

        return $total;
    }

}
