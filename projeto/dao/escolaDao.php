<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of escolaDao
 *
 * @author Renat
 */


class escolaDao {

    //put your code here
    private $con;

    function __construct() {
        $this->con = new Conexao();
    }

    public function consultar($codigoua) {
        $con = $this->con->getConexao();

        $condicional = "";

        if (strlen($codigoua)) {
            $condicional = " WHERE UA = $codigoua ";
        }

        $dadosArray = "";

        $sql = "SELECT A.*,B.telefone FROM ESCOLA A LEFT JOIN ESCOLATELEFONE B ON A.ID = B.ESCOLAID $condicional ORDER BY NOME";

        $stmp = $con->prepare($sql);
        $stmp->execute();

        $resultado = $stmp->fetchAll();

        $entidades = null;

        foreach ($resultado as $value) {

            $escola = new escola($value['nome'], $value['id'], $value['ua'], $value['email'], $value['telefone'], $value['caixa'],"","","");

            $entidades[] = $escola;
        }

        return $entidades;
    }

    public function salvar($entidade) {
        $con = $this->con->getConexao();
        
        $stmt = $con->prepare("INSERT INTO ESCOLA("
                . "nome,"
                . "ua,"
                . "caixa,"
                . "email,"
                . "endereco,"
                . "bairro,"
                . "cidade) "
                . " VALUES ("
                . "?,"
                . "?,"
                . "?,"
                . "?,"
                . "?,"
                . "?,"
                . "?)");
        
        $nome = $entidade->getNome();
        $ua = $entidade->getUa();
        $caixa = $entidade->getCaixa();
        $email = $entidade->getEmail();
        $endereco = $entidade->getEndereco();
        $bairro = $entidade->getBairro();
        $cidade = $entidade->getCidade();
        
        $stmt->bindParam(1,$nome);
        $stmt->bindParam(2,$ua);
        $stmt->bindParam(3,$caixa);
        $stmt->bindParam(4,$email);
        $stmt->bindParam(5,$endereco);
        $stmt->bindParam(6,$bairro);
        $stmt->bindParam(7,$cidade);
        
        $res = $stmt->execute();
        
        return $res;
        
    }

}
