<!doctype html>

<?php
include './dao/Conexao.php';
include './dominio/escola.php';
include './dao/escolaDao.php';
include './dao/escolaServidorDao.php';
include './dominio/servidor.php';

$escolaDao = new escolaDao();

$escolaServidorDao = new escolaServidorDao();
$mensagemErro = "";
?>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">


        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>



        <!-- JS dependencies -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <script
            src="https://code.jquery.com/jquery-3.5.1.js"
            integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
        crossorigin="anonymous"></script>

        <script src="js/jquery.table2excel.js"></script>


        <title>Lista Sodexo</title>
    </head>
    <body>

        <div class="container" style="width: 80%;margin: auto">


            <div class="jumbotron">
                <h1>Servidores Sodexo</h1>
                <p>Cria uma tabela para ser utilizada na mala direta.</p>
            </div>


            <form class="form-inline" >
                <div class="form-group">
                    <label for="codigoua"> Código UA:</label>
                    <input type="codigoua" class="form-control" name="codigoua" required="required" id="codigoua">
                </div>
                <div class="form-group">
                    <label for="nome">nome:</label>
                    <input type="nome" class="form-control" name="nome" required="required" id="nome">
                </div>
                <input type="button" onclick="adicionarServidor()" class="btn btn-info" value="Adicionar" style="margin-left: 10px" >

            </form>






            <div class="alert alert-danger" role="alert" style="margin-top: 30px;display: none" id="erroPreenchimentoCodigoUa" >
                Preencha o campo código UA
            </div>

            <div class="alert alert-danger" role="alert" style="margin-top: 30px;display: none" id="erroPreenchimentoNome" >
                Preencha o campo Nome
            </div>



            <script>

                $(document).ready(function () {
                    /*$("#table2excel").table2excel({
                     exclude: ".noExl",
                     name: "Worksheet Name",
                     filename: "SomeFile",
                     fileext: ".xls",
                     preserveColors: true
                     });
                     
                     $("#table2excel").table2excel({
                     exclude: ".noExl",
                     name: "Worksheet Name",
                     filename: "SomeFile",
                     fileext: ".xls",
                     exclude_img: true,
                     exclude_links: true,
                     exclude_inputs: true
                     });*/
                });


                function exportarTabelaExcel() {
                    $("#table2excel").table2excel({
                        // exclude CSS class
                        exclude: ".noExl",
                        name: "Worksheet Name",
                        filename: "maladireta", //do not include extension
                        fileext: ".html" // file extension
                    });
                }




            </script>

            <input onclick="excluirBase()" class="btn btn-danger" value="Limpar base" style="margin-top: 50px">
            <input onclick="exportarTabelaExcel()" class="btn btn-success" value="Exportar Excel" style="margin-top: 50px">

            <script>

                function adicionarServidor() {

                    erro = 0;

                    if ($('#codigoua').val().length === 0) {
                        $('#erroPreenchimentoCodigoUa').show();
                        erro++
                    }

                    if ($('#nome').val().length === 0) {
                        $('#erroPreenchimentoNome').show();
                        erro++
                    }

                    if (erro > 0) {
                        return;
                    }


                    $.post("adicionarServidor.php", {nome: $('#nome').val(), codigoua: $('#codigoua').val()}, function (data, status) {
                        if (data.length > 0) {
                            alert(data);
                        }

                    });

                    location.reload();
                }

                function excluirBase() {
                    if (confirm("Quer realmente excluir a base?!")) {
                        $.post("excluirservidores.php", function (data, status) {
                            alert(data);
                        });

                        location.reload();
                    }
                }
            </script>


            <table class="table" style="margin-top: 50px" id="table2excel">
                <thead>
                    <tr>
                        <th  class="noExl"></th>
                        <th>Nome</th>
                        <th>Escola</th>
                        <th>Email</th>
                        <th>Caixa</th>
                        <th  class="noExl">#</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $servidores = $escolaServidorDao->consultar();

                    if (is_array($servidores)) {

                        $contador = 0;

                        foreach ($servidores as $servidor) {

                            $escola = $servidor->getEscola();

                            $nomeEscola = $escola->getNome();
                            $nome = $servidor->getNome();
                            $email = $escola->getEmail();
                            $caixa = $escola->getCaixa();
                            $contador++;
                            ?>
                            <tr>
                                <td  class="noExl"><?php echo $contador ?></td>
                                <td><?php echo $nome ?></td>
                                <td><?php echo $nomeEscola ?></td>
                                <td><a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></td>
                                <td><?php echo $caixa ?></td>
                                <td  class="noExl"><form class="form-inline" action="excluirservidor.php" method="post">
                                        <input type="hidden" class="form-control" name="idescolaservidor" value="<?php echo $servidor->getId() ?>" >
                                        <input type="submit" class="btn btn-danger" value="Excluir" >

                                    </form></td>             


                            </tr>

                            <?php
                        }
                    }

                    /*    $dados['escola'] = "Escola teste";
                      $dados['nome'] = $_POST['nome'];
                      $dados['email'] = "emailteste@teste.com.br";
                      $dados['caixa'] = "11"; */




                    /* if($_GET){
                      echo "tem sessao" + $_GET["nome"];
                      }else{
                      var_dump($_GET);
                      } */
                    ?>
                </tbody>
                <!--<tbody>
                  <tr>
                    <td>John</td>
                    <td>Doe</td>
                    <td>john@example.com</td>
                  </tr>
                  <tr>
                    <td>Mary</td>
                    <td>Moe</td>
                    <td>mary@example.com</td>
                  </tr>
                  <tr>
                    <td>July</td>
                    <td>Dooley</td>
                    <td>july@example.com</td>
                  </tr>
                </tbody>-->
            </table>
        </div>
    </body>

    <footer>    
        <!-- Copyright -->
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
            © 2020 Copyright:
            <a class="text-dark" href="https://mdbootstrap.com/">Núcleo de Frêquencia e Pagamento - Mogi das Cruzes</a>
        </div>
        <!-- Copyright -->
    </footer>
</html>