<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include './dao/Conexao.php';
include './dao/escolaDao.php';
include './dominio/escola.php';
include './dominio/servidor.php';
include './dao/escolaServidorDao.php';

$escolaDao = new escolaDao();
$escolaServidorDao = new escolaServidorDao();

$escola = $escolaDao->consultar($_POST['codigoua']);

$mensagem = "";

if (!is_array($escola)) {
    $mensagem = "Escola não localizada!";
} else {
    $servidor = new servidor($_POST['nome'], $escola[0]);

    $escolaServidorDao->salvar($servidor);
}